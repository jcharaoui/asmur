#!/bin/sh

# asmur.sh
#
# Aegir site modules update report
#
# Copyright: Jerome Charaoui <jerome@cmaisonneuve.qc.ca>
# License: GPL-3


tmpfile=`mktemp`

for platform_path in $(ls -1d /var/aegir/platforms/*/); do
    if [ ! -e ${platform_path}sites ]; then
        continue
    fi
    platform="$(basename $platform_path)"
    title="Platform $platform"
    echo $title >> $tmpfile
    echo $title | sed -e 's/./\=/g' >> $tmpfile
    echo >> $tmpfile
    for site_path in $(ls -1d ${platform_path}sites/*/); do
        site="$(basename $site_path)"
        if [ "$site" == "all" ] || [ "$site" == "default" ] || [ ! -e ${site_path}settings.php ]; then
            continue
        fi
        cd $site_path
        drush en update -y -q
        drush vset update_check_frequency 1 -y -q
        drush vset update_check_disabled 0 -y -q
        drush vdel update_notify_emails -y -q
        drush vset update_notification_threshold 'all' -y -q
        updates=$(drush -n -p up)
        table=
        echo $site >> $tmpfile
        echo $site | sed -e 's/./\-/g' >> $tmpfile
        if [ -n "$updates" ]; then
            echo "$updates" | column -t | sed -e 's/^/\* /' >> $tmpfile
        else
            echo "None found." >> $tmpfile
        fi
        echo >> $tmpfile
    done
done

if [ -z $1 ]; then
    cat $tmpfile
else
    cat $tmpfile | mail -s "Aegir site module update report" $1
fi

rm $tmpfile
